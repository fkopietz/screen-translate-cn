# screen-translate-cn

This docker image listens for `.png` files created in `$SCREENSHOT_DIR` directory, performs a recognition of the Chinese language and sends the output to Amazon Translate for translation. It prints the translated text to console.

## Usage
Put credentials and configuration for Amazon Web Services as described in the [Boto 3 Documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html#configuration) in the `~/.aws` directory.

Run this container with:
`docker run -t -v $SCREENSHOT_DIR:/source:ro -v ~/.aws:/root/.aws:ro registry.gitlab.com/fkopietz/screen-translate-cn`

Replace `$SCREENSHOT_DIR` with the directory where screenshots are saved.
