#!/usr/bin/env python3

import boto3
import sys
from tesserocr import PyTessBaseAPI, PSM
import time


from watchdog.events import PatternMatchingEventHandler
from watchdog.observers import Observer


class FileHandler(PatternMatchingEventHandler):
    patterns = ["*.png"]

    def process(self, event):
        with PyTessBaseAPI(lang='eng+chi_sim', psm=PSM.SINGLE_LINE) as api:
            api.SetImageFile(event.src_path)
            chinese_text = api.GetUTF8Text()

            client = boto3.client('translate')
            response = client.translate_text(
                Text=chinese_text,
                SourceLanguageCode='zh',
                TargetLanguageCode='en')

            english_text = response['TranslatedText']

            print(chinese_text.rstrip() + ' - ' + english_text)


    def on_created(self, event):
        self.process(event)


if __name__ == "__main__":
    args = sys.argv[1:]
    observer = Observer()
    observer.schedule(FileHandler(), path=args[0] if args else '/source')
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
