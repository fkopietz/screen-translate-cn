FROM ubuntu:18.04

# Set the locale

RUN apt-get update && apt-get install -y \
      python3 \
      python3-pip \
      tesseract-ocr \
      tesseract-ocr-chi-sim \
      tesseract-ocr-chi-tra \
      libtesseract-dev \
      libleptonica-dev \
      locales \
  && rm -rf /var/lib/apt/lists/*

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
ENV PYTHONIOENCODING=utf-8

RUN pip3 install tesserocr watchdog boto3

RUN mkdir /source && mkdir /home/translator/

COPY ./translate-runner.py /home/translator/

ENTRYPOINT ["python3", "/home/translator/translate-runner.py", "/source"]
